

class: center, middle

# Kubernetes workshop

> .quote[A pod is like a Fedex box. It doesn’t matter what’s inside it. As long as there’s a Fedex box, Fedex knows what to do with it.]
> .footer[Kelsey Hightower AKA Sweet Baby Kubernetes Jesus]

---

# What is Kubernetes

If you want to run docker containers on a bunch of servers, Kubernetes can orchestrate the placement for you.

---

.center[![kubernetes overview](https://deis.com/images/blog-images/kubernetes-overview-1-0.png)]

---

![](https://s3.amazonaws.com/media-p.slid.es/uploads/803673/images/4437124/CNCF__Serverless___Nuclio__Read-Only_.png)

---

## Running a cluster with docker-for-mac
- Install `kubectl` with home-brew
- Install docker for Mac
- In the Preferences pane, enable Kubernetes

```
kubectl config use-context docker-for-desktop
```
```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```
```
kubectl port-forward kubernetes-dashboard-845747bdd4-2nq4h 8443:8443 --namespace=kube-system
```

- https://localhost:8443

---

layout: false
.left-column[
  ## k8s jargon
  ### k8s
]
.right-column[
## Getting familiar with k8s jargon
**k8s**: shortcut for kubernetes :P
]

---

layout: false
.left-column[
  ## k8s jargon
  ### k8s
  ### Node
]
.right-column[
## Getting familiar with k8s jargon
**Node**: an actual computer (or VM) that is running a part of the k8s cluster
]

---

layout: false
.left-column[
  ## k8s jargon
  ### k8s
  ### Node
  ### Cluster
]
.right-column[
## Getting familiar with k8s jargon
**Cluster**: a group of nodes
]

---

layout: false
.left-column[
  ## k8s jargon
  ### k8s
  ### Node
  ### Cluster
  ### Master
]
.right-column[
## Getting familiar with k8s jargon
**Master**: a single or group of nodes that can schedule containers on worker nodes
]
---

layout: false
.left-column[
  ## k8s jargon
  ### k8s
  ### Node
  ### Cluster
  ### Master
  ### Worker
]
.right-column[
## Getting familiar with k8s jargon
**Worker**: a group of nodes that are solely dedicated to running docker containers
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Container
]
.right-column[
## Getting familiar with k8s jargon
**Container**: a single instance of a running docker container
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Container
  ### Pod
]
.right-column[
## Getting familiar with k8s jargon
**Pod**: a number of tightly coupled running containers that make up a single service
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Container
  ### Pod
  ### Deployment
]
.right-column[
## Getting familiar with k8s jargon
**Deployment**: a definition of how to deploy a group of pods
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Container
  ### Pod
  ### Deployment
  ### Namespace
]
.right-column[
## Getting familiar with k8s jargon
**Namespace**: a contained group within a cluster in which to run services (can be used for  security separation, or things like DTAP)
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Container
  ### Pod
  ### Deployment
  ### Namespace
  ### Secrets
]
.right-column[
## Getting familiar with k8s jargon
**Secrets**: environment variables that are stored within the cluster as base64 encoded values, so you don’t have to pass the actual value to the pod in the pod definition
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Service
]
.right-column[
## Getting familiar with k8s jargon
**Service**: an abstraction and access layer for multiple pods that make up the same service. Provides a single entry point for other services
]

---

layout: false
.left-column[
  ## k8s jargon
  ### Service
  ### Ingress
]
.right-column[
## Getting familiar with k8s jargon
**Ingress**: Rules about how to route incoming traffic from outside of the cluster to a service. Usually handled by webservers such as HAProxy, nginx or traefik, and can be done on a host & path base
]

---

class: center, middle

## Is your head spinning yet?

---

## Running something on your Kubernetes cluster
```bash
kubectl cluster-info
```
```
kubectl get nodes
```
```
kubectl get pods
```

---

## Running something on your Kubernetes cluster
```
kubectl run alpine --image=alpine:3.6 -- echo "Hello World"
```

--

In the dashboard, go to Pods -> alpine and see why it failed. Take a look at the logs.

--

```
kubectl delete deployment/alpine
```

---

## Running something on your Kubernetes cluster
```
kubectl run alpine --image=alpine -i -t
```
In a different tab:
```
kubectl get pods
```
```
kubectl get deployments
```

--

In the dashboard, go to Pods -> alpine
Look at the scheduler in the event log, it scheduled the pod on a node

--

```
kubectl delete deployment/alpine
```

---

## Running a service
Just as with docker, running commands randomly does not scale. So k8s works with definition files in yaml.
You can see them in the dashboard for existing services.

---

Create echo.yaml:
```yaml
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  namespace: default
  name: echo
spec:
  replicas: 2
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: echo
    spec:
      containers:
      - name: echo
        image: gcr.io/google_containers/echoserver:1.4
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
          protocol: TCP
```
```
watch kubectl get pods
```
```
kubectl apply -f echo.yaml
```

---

## Running a service
We can see the pods are running, but there is no way to access them yet. Even the docker process doesn’t export any open ports (you can check this with `docker ps`). We need a service (definition) to access it.

---

## Running a service
Add this to echo.yaml:
```yaml
---
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: echo-service
  labels:
    app: echo-service
spec:
  selector:
    app: echo
  ports:
  - port: 8080
    targetPort: 8080
    protocol: TCP
  type: NodePort
```
```
kubectl apply -f echo.yaml
```

---

## Running a service
```
kubectl get svc
```
```
curl http://localhost:32730
```

---

## Service types
- NodePort: open an port on the actual node the container is running on
- ClusterIP: open a port within the cluster. Assigns a new ip address to the service so multiple services can use the same ports. Service will only be accessible for services within the same cluster
- LoadBalancer: Same as ClusterIP, but sets up a cloud load balancer (does not work locally)

**Exercise**: (15 min)  Change the service definition to `ClusterIP` and try to curl the service from another pod (like alpine)

---

## Convert docker-compose to kubernetes definition, a tale of 2 services

Suppose you're using this docker-compose file locally to developer a service that uses a database:
```yaml
version: "3"
services:
    php:
        image: pascaldevink/workshop:latest
        ports:
            - "4000:4000"
        depends_on:
            - mysql
        environment:
            - DATABASE_URL=mysql://blog:golb@mysql:3306/blog?charset=utf8mb4&serverVersion=5.7
    mysql:
        image: mysql:5.7.20
        environment:
            MYSQL_ROOT_PASSWORD=toor
            MYSQL_DATABASE=blog
            MYSQL_USER=blog
            MYSQL_PASSWORD=golb
```

Let's convert it to kubernetes service definitions...

---

database.yaml:
```yaml
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  namespace: default
  name: mysql
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - name: mysql
        image: mysql:5.7.20
        imagePullPolicy: Always
        ports:
        - containerPort: 3306
          protocol: TCP
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: toor
        - name: MYSQL_USER
          value: blog
        - name: MYSQL_PASSWORD
          value: golb
        - name: MYSQL_DATABASE
          value: blog
```    

---

database.yaml (continued):
```yaml
---
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: mysql
  labels:
    app: mysql
spec:
  selector:
    app: mysql
  ports:
  - port: 3306
    targetPort: 3306
    protocol: TCP
  type: ClusterIP

```

---

## Convert docker-compose to kubernetes definition, a tale of 2 services

**Exercise**: (20 min) with this database template and the previous example, create a definition for the php application.

*Hint*: it’s best to keep the number of replicas to 1, as the image deletes and creates the database each time it runs.

---

## Secrets
In the previous exercise, both services had environment variables that might change, but now they are set in the pod definition. This makes the definition inflexible and also exposes the credentials to anybody who can read the pod definition. Not an ideal situation.

Enter secrets.

---

## Secrets

Secrets can not be created by a yaml definition, because that would defeat the purpose. Instead, you need to create them using kubectl directly (we’ll look at other ways later):
```
kubectl create secret generic workshop-kubernetes --from-literal="DATABASE_URL=mysql://blog:golb@mysql:3306/blog?charset=utf8mb4&serverVersion=5.7"
```

---

## Secrets

You can then use this secret by changing the pod definition a little bit from:
```yaml
        env:
        - name: DATABASE_URL
          value: mysql://blog:golb@mysql:3306/blog?charset=utf8mb4&serverVersion=5.7
```
To
```yaml
        envFrom:
        - secretRef:
            name: workshop-kubernetes
```

---

## Kubectl sucks

- Kubectl is powerful

???
Kubectl is a very powerful command, with which you can control an entire Kubernetes cluster.

--

- Tiresome to remember

???
However, it it also tiresome to remember all the commands, specially if all you want is to just deploy your application. Constantly applying new configurations, manually creating secrets, etc. can become hard to manage and scale.

--

- Helm!

???
Helm is a package manager for Kubernetes, which makes it easy to package all the definitions into a “Chart”. A chart can be installed and upgraded and can have dependencies. Thereby making it very easy to use and maintain services in Kubernetes.

---

## Install and init helm
- Install helm using brew `brew install kubernetes-helm`
- Initialize the server-side component, tiller with `helm init`
- Make sure you have the latest repository information with `helm repo update`

The server-side component is needed to not depend on 1 client to manage the cluster.

---

## Install the MySQL chart
Using this single command, we can install MySQL, without having to create all the yaml definitions:
```
helm install stable/mysql --set mysqlRootPassword=toor,mysqlUser=blog,mysqlPassword=golb,mysqlDatabase=blog
```

Helm will take care of all the secrets that need to be created.

---

## Install the MySQL chart

**Exercise**: Replace the previous MySQL installation with the helm chart, and update the workshop-kubernetes service to use that installation

---

## *bonus round* A real-world example

The FX service is a real-world example of a TicketSwap service that has been packaged as a chart using helm.

First, create the `backend` namespace
```
kubectl create namespace backend
```
Then, create a secret for pulling private images from docker hub:
```
kubectl create secret docker-registry ticketswap-registry --docker-username="<username>" --docker-password="<password>" --docker-email="<email>" --docker-server="https://index.docker.io/v1/"
```

Then, easily install the FX like this:
```
git clone git@github.com:TicketSwap/fx.git
cd fx
helm install etc/charts/fx --name=fx --namespace=backend --set  fx.serviceType=NodePort,fx.stripe_api_key=sk_test_BkRUJLmPCKk947f43v9fXcPq
```

Once it’s installed, you can use it like this: `curl localhost:30799/v1/convert/EUR/USD`
